# alfabet #

This is a demonstration of a "virtual array": in this case,
an array of all numbers that can be written in words in Dutch.
The Dutch language allows two write out numbers up to 10**66.
Of course, such an array would never fit in your computer!
Unfortunately, the progam is written in Python 2,
and commented in Dutch. Sorry about that, this may be fixed sometime.

### What is this for? ###

It all started with the question: what is the first number, alphabetically?
In Dutch, it is 8 (acht)
Then things got out of hand.
So, the second number is 8000000000000 (achtbiljard),
and the 10**50 number is: 8008038038038887077038038038000000000000000048726008000818756776
(achtdeciljardachtdeciljoenachtendertig...)

### How do I get set up? ###

Since is it not a serious program, I would say:
```
>>> import alfabet
>>> a = alfabet.alphaNumbers
>>> a[0]
Number(8,"acht")
>>> a.bisect("jurjen")
331331330330330330331331331331330330330330330330331331330330331331L
>>> a[_]
Number(1000000000,"miljard")
>>> a[a.bisect("jurjen") - 1]
Number(107000000000070070070,"honderdzeventriljoenzeventigmiljoenzeventigduizendzeventig")
```
So, my first name fits alphabetically between
honderdzeventriljoenzeventigmiljoenzeventigduizendzeventig and miljard
in the list of all numbers,
at position 331331330330330330331331331331330330330330330330331331330330331331

### Contribution guidelines ###

I hope to have time for the following improvements, rougly in this order:
- Port to Python 3
- Comments in English
- Include the grammar for English numbers (we'll have to think hard about spaces)
- Make the grammar a nice van Wijngaarden two level grammar

Tell me if you are interested.

### Who do I talk to? ###

Jurjen N.E. Bos
"""Maak een (virtueel) alfabetisch array van alle getallen in het Nederlands.
De details staan nog even ergens anders; dit wordt als het goed is er bij gezet.
Het gaat om de class Number en de class AlphaNumbers.
De instantie alphaNumbers van AlphaNumbers gedraagt zich als een lijst van 10**66 Numbers
op alfabetische volgorde.
Je kan een getal opzoeken met bisect(s): geeft het eerste getal >= s (string).
De class Number(integer=None, string=None) geeft een getal als string en als int
weer. Je hoeft maar een van de twee mee te geven. Pas op: als je allebei meegeeft,
controleert hij niks!
"""
from sys import stdout
from operator import xor

####Definities zodat we een graaf kunnen maken-----------------------------
class Point:
    """Een punt van de graaf die de grammatica definieert van een getal.
    Een state van de statemachine bestaat uit een Point een een lengte:
    het aantal cijfers van het getal, aangegeven in "digits".
    Occurrence is het aantal paden vanaf hier naar de state Stop;
    deze berekeningen worden gebruikt als we van point naar state overgaan.
    transitions zijn overgangen naar andere states
    """
    def __init__(self, name, occurrence=None, checkCondition="True"):
        """occurrence evalueert tot het aantal getallen dat gemaakt kan worden
        checkCondition evalueert tot de voorwaarde waarin deze state voorkomt.
        Te gebruiken in de expressies:
        digits: aantal cijfers van het getal
        s: (10**digits-1)//999
        """
        self.name = name
        if occurrence is None: self.occurrence = None
        else: self.occurrence = compile(occurrence,occurrence,'eval')
        self.checkCondition = compile(checkCondition, "state condition", 'eval')
        self.transitions = []

    def appendTransition(self, **kwargs):
        """Voeg een state transitie toe.
        (Dit is de enige manier om een Point object te veranderen,
        verder worden ze als een non-mutable gezien.)
        """
        self.transitions.append(PointTransition(**kwargs))

    def __repr__(self):
        return 'point %s (strings: %s)'%(
            self.name,
            ','.join(self.getAllTransitionStrings()))

    def getTransitions(self, string):
        """geef alle overgangen die een gegeven string kunnen produceren.
        De voorwaarden worden niet gecontroleerd."""
        for t in self.transitions:
            if string in t.strings: yield t

    def getAllTransitionStrings(self):
        strings = set()
        for t in self.transitions: strings.update(t.strings)
        return strings

    def printNewPoints(self):
        #Bepaal alle nieuwe punten die je kan bereiken;
        #nulovergangen worden eruit gehaald
        #we houden per beginstring een lijst bij van de punten waar je komt
        #in paren (uitlegString, Point)
        #begin met alleen een nulovergang naar mezelf
        nextPerString = {"":[("",self)]}
        while nextPerString[""]:
            explanation, point = nextPerString[""].pop()
            for t in point.transitions:
                for string in t.strings:
                    nextPerString.setdefault(string,[]).append((
                        explanation and explanation+"->"+point.name or ".",
                        t.nextPoint))
        del nextPerString[""]
        #druk de overgangen netjes af als volgt:
        #string:  point1 (explanation)
        #         point2 (explanation)
        print "Overgangen vanaf %s%s"%(
            self.name,
            self.occurrence
                and "(%s mogelijkheden)"%self.occurrence.co_filename
                or "")
        for string in sorted(nextPerString):
            linestart = string.ljust(10)
            lines = [
                '%10s %-30s %s'%(
                    point.name,
                    '('+explanation+')',
                    point.occurrence.co_filename)
                for explanation,point in nextPerString[string]]
            lines.sort()
            for line in lines:
                print linestart+line
                linestart=' '*10

class PointTransition:
    """De overgang van een point naar een andere point.
    Een lijntje in de graaf.
    """
    def __init__(self, nextPoint,
            condition = "True", strings = [""], nextDigits = "digits", value = ""):
        """Maak een nieuwe point.
        Het is erg onverstandig om de volgorde van de parameters te gebruiken;
        geen mens kan het dan meer lezen.
        Aanbevolen volgorde:
            condition: evalueert tot de voorwaarde waaronder
                de overgang is toegestaan
                (uitdrukking in digits en index (van de string in strings))
            strings: lijst van strings die bij de overgang horen
            nextPoint: naam van punt waar hij heen gaat
            nextDigits: evalueert tot de waarde van digits die hierbij hoort
                (uitdrukking in digits en index)
            value: wat je moet doen om het getal uit te rekenen
                (bewerkt mantissa, exponent, en total
                 als functie van digits en index)
        """
        assert isinstance(condition,str), "condition moet compileerbaar zijn"
        self.condition = compile(condition, 'point condition', 'eval')
        assert isinstance(strings,list), "strings moet een lijst zijn"
        self.strings = strings
        assert isinstance(nextPoint,Point), "nextPoint moet een Point zijn"
        self.nextPoint = nextPoint
        assert isinstance(nextDigits,str), "nextDigits moet compileerbaar zijn"
        self.nextDigits = compile(nextDigits, 'point digits', 'eval')
        assert isinstance(value,str), "nextDigits moet compileerbaar zijn"
        self.value = compile(value, 'point value computation', 'exec')

    def __repr__(self):
        return 'transition '+','.join(self.strings)

####Stap 1: definieer de graaf van de grammatica-----------------------------
"""
Het linkerstuk van het plaatje, vanaf Getal, is gemaakt om jaartallen
mogelijk te maken (bijvoorbeeld negentienhonderdvierenzestig).
Er is steeds maar een manier om op een gegeven plek in het plaatje te komen.
Sprongen tussen ronde haakjes, naar een label:
    1: Nahonderd, 2: GeenDuizend, 3: GeenEeuw, 4: Ziljoen
Hoofdletters in dit plaatje zijn pointnamen.
2-9* staat voor twinTmNegen: twin, der, veer, ..., en 3-9* eensgelijks

    Getal                                Grond--+---\
      |                                    |    |   |
      |                                    |    |  2-9
      +-----+-----+----+-----+----+--(3)   |    |   |           1: Nahonderd
      |     |     |    |     |    |        |    \---+-->honderd-----------\
     1-9   3-9*  elf   |    2-9  nul       |                              |
      |     |   twaalf |     |    |        |       Tweecijfer             |
 /----+    tien   |    +--+--+   Stop      +--->+-------+---+---+----+<---+---\
 |    |     |     |    |  |  |             |    |       |   |   |    |    |   |
Stop  en    |<----/    | (2) |             |   1-9   3: +   |  2-9   |    |  een
      |     |          |     |             |    |  Geen-|   |   |   elf   |   |
     2-9*   |      honderd  duizend        |    en  Eeuw|  3-9* |  twaalf |   |
      |     |          |      |            |    |       |   |   |    |    |   |
     tig    |         (1)    (1)           |    |<------+-->|   |    |    |   |
      |     |                              |    |           |   |    |    |   |
      +-----+JaartalOfZiljoen              |   2-9*        tien |    |    |   |
      |     |                              |    |           |   |    |    |   |
   honderd  |                              |   tig          |   |    |    |   |
      |    (4)                             |    |           |   |    |    |   |
     (1)                  Stop<----een-----+    |<----------<---<----<----<---/
  [3 digits]                               |    |
                                           |  4:| Ziljoen
                                           |    |
                                           |    +--->Stop
                                           |    |
                                  Stop<----+--->+---duizend--->Grond
                                                |
                                             2: | GeenDuizend
                                                |
                                              m-dec (als digits>6*(index+1)
                                                |
                                               ilj
                                                |
                                             oen/ard
                                                |
                                              Grond
"""

#---Hulpwaarden
longestSubstring = 7 #honderd en duizend zijn het langst

derTmNegen =         ["der", "veer","vijf","zes","zeven","acht","negen"]
tweeTmNegen = ["twee","drie","vier","vijf","zes","zeven","acht","negen"]
twinTmNegen = ["twin","der", "veer","vijf","zes","zeven","tach","negen"]

duizend = ["duizend"]
een = ["een"]
elfTwaalf = ["elf","twaalf"]
en = ["en"]
honderd = ["honderd"]
ilj = ["ilj"]
niks = [""]
nul = ["nul"]
oenard = ["oen","ard"]
tien = ["tien"]
tig = ["tig"]
ziljoenen = ["m","b","tr","quadr","quint","hex","hept","oct","non","dec"]

#---punten-------------------------------------------------------------
#volgens het plaatje. Meestal geeft de naam aan wat op dat punt wordt verwacht
#alle getallen
Getal = Point(name = "Getal", occurrence="10**digits")
#er is alleen nog een cijfer geweest
BeginEn = Point(name = "BeginEn",occurrence="8*s+801")
#er is alleen een cijfer geweest, en "en"
BeginBoeltig = Point(name = "BeginBoeltig",occurrence="8*s+800")
#er is alleen "(cijfer)en(cijfer)" geweest (nu komt "tig" natuurlijk)
BeginTig = Point(name = "BeginTig",occurrence="s+100")
#er is een getal ..en..tig geweest, nu komt "honderd" voor jaartal
JaartalOfZiljoen = Point(name = "JaartalOfZiljoen",occurrence="s+100")
#er is een beginstuk van een tiental geweest
BeginTien = Point(name = "BeginTien",occurrence="s+100")
#Dit kan een duizendtal worden (zonder honderdtal); betekent niks voor 3 cijfers
HonderdOfDuizend = Point(
    name = "HonderdOfDuizend",
    occurrence="101*s-901",
    checkCondition="digits>3")

#alle getallen als achterstuk van een groter getal
Grond = Point(name = "Grond", occurrence="10**digits")

#Een hondertal begint
Honderd = Point(name = "Honderd", occurrence="100*s")
#Twee cijfers na een hondertal
Nahonderd = Point(name = "Nahonderd",occurrence="100*s")
#Tussenstappen
En = Point(name = "En",occurrence="8*s")
Boeltig = Point(name = "Boeltig",occurrence="8*s")
Tig = Point(name = "Tig",occurrence="s")
#Dit kan vanaf het begin van een getal komen, dan is het zeker geen eeuwaantal
GeenEeuw = Point(name = "GeenEeuw",occurrence="9*s")

Tien = Point(name = "Tien",occurrence="s")
#Twee cijfers los: lijkt op honderd, maar "een" komt niet voor en "" betekent 1
Tweecijfer = Point(name = "Tweecijfer",occurrence="98*s")
#als hij de ziljoen gaat genereren
ZiljoenOfStop = Point(name = "ZiljoenOfStop", occurrence="s")
Ziljoen = Point(name = "Ziljoen",occurrence="s-1")
GeenDuizend = Point(name = "GeenDuizend", occurrence="max(0,s-1001)")

Ilj = Point(name = "Ilj",occurrence="(10**digits-10**((digits-3)//6*6))//999")
OenOfArd = Point(
    name = "OenOfArd",
    occurrence="(10**(digits)-10**((digits-3)//6*6))//999")

#klaar
Stop = Point(name = "Stop", occurrence="1")

#---Overgangen---------------------------------------------------------------
#het getal is nog leeg
#als het begint met ..en..tig, kan je nog beide kanten op
Getal.appendTransition(
    strings=een+tweeTmNegen,
    nextPoint=BeginEn,
    value="mantissa=index+1")
BeginEn.appendTransition(strings=en, nextPoint=BeginBoeltig)
BeginEn.appendTransition(nextPoint=Stop,value='total=mantissa')
BeginBoeltig.appendTransition(
    strings=twinTmNegen,
    nextPoint=BeginTig,
    value="mantissa+=10*(index+2)")
BeginTig.appendTransition(strings=tig, nextPoint=JaartalOfZiljoen)

#als het begint met ..tien, kan je ook nog beide kanten op
Getal.appendTransition(
    strings=derTmNegen,
    nextPoint=BeginTien,
    value="mantissa=index+3")
BeginTien.appendTransition(
    strings=tien,
    nextPoint=JaartalOfZiljoen,
    value="mantissa+=10")

Getal.appendTransition(
    strings=elfTwaalf,
    nextPoint=JaartalOfZiljoen,
    value="mantissa=index+11")

#als het begint met ..duizend, mag je geen honderdtallen meer
Getal.appendTransition(
    strings=niks+tweeTmNegen,
    nextPoint=HonderdOfDuizend,
    value="mantissa=index+1")
HonderdOfDuizend.appendTransition(
    strings=honderd,
    nextPoint=Nahonderd,
    value="mantissa*=100")
HonderdOfDuizend.appendTransition(
    strings=duizend,
    nextPoint=Nahonderd,
    nextDigits="3",
    value="mantissa*=1000")
HonderdOfDuizend.appendTransition(nextPoint=GeenDuizend)

#en anders wordt het zeker geen jaartal
Getal.appendTransition(strings=nul,nextPoint=Stop,value="total=0")
Getal.appendTransition(nextPoint=GeenEeuw,value="mantissa=0")

#hier kan het nog een jaartal worden: neem de beslissing en haak in op de stroom
JaartalOfZiljoen.appendTransition(
    strings=honderd,
    nextPoint=Nahonderd,
    nextDigits="3",
    value="mantissa*=100")

JaartalOfZiljoen.appendTransition(nextPoint=ZiljoenOfStop)

#en nu de rechterkant van het plaatje; zoveel mogelijk van links naar rechts
Grond.appendTransition(
    nextPoint=Ziljoen,
    value="mantissa=1")
Grond.appendTransition(nextPoint=Stop)
Grond.appendTransition(
    strings=een,
    nextPoint=Stop,
    value="total+=1")
Grond.appendTransition(nextPoint=Tweecijfer, value="mantissa=0")

#honderdtallen
Grond.appendTransition(
    strings=niks+tweeTmNegen,
    nextPoint=Honderd,
    value="mantissa=index+1")
Honderd.appendTransition(
    strings=honderd,
    nextPoint=Nahonderd,
    value="mantissa*=100")

Nahonderd.appendTransition(nextPoint=Tweecijfer)
Nahonderd.appendTransition(
    strings=niks+een,
    nextPoint=ZiljoenOfStop,
    value="mantissa+=index")

Tweecijfer.appendTransition(
    strings=een+tweeTmNegen,
    nextPoint=En,
    value="mantissa+=index+1")
Tweecijfer.appendTransition(strings=niks, nextPoint=GeenEeuw)
Tweecijfer.appendTransition(
    strings=derTmNegen,
    nextPoint=Tien,
    value="mantissa+=index+3")
Tweecijfer.appendTransition(
    strings=tweeTmNegen,
    nextPoint=ZiljoenOfStop,
    value="mantissa+=index+2")
Tweecijfer.appendTransition(
    strings=elfTwaalf,
    nextPoint=ZiljoenOfStop,
    value="mantissa+=index+11")

GeenEeuw.appendTransition(nextPoint=Boeltig)
GeenEeuw.appendTransition(nextPoint=Tien)

#hulppunten
En.appendTransition(strings=en, nextPoint=Boeltig)

Boeltig.appendTransition(
    strings=twinTmNegen,
    nextPoint=Tig,
    value="mantissa+=10*(index+2)")

Tien.appendTransition(
    nextPoint=ZiljoenOfStop,
    strings=tien,
    value="mantissa+=10")

Tig.appendTransition(strings=tig, nextPoint=ZiljoenOfStop)

#de exponentberekening: ziljoenen
ZiljoenOfStop.appendTransition(nextPoint=Ziljoen)
ZiljoenOfStop.appendTransition(
    nextPoint=Stop,
    value='total+=mantissa')
Ziljoen.appendTransition(nextPoint=GeenDuizend)
Ziljoen.appendTransition(
    strings=duizend,
    condition="digits>3",
    nextPoint=Grond,
    nextDigits="3",
    value="total+=mantissa*1000")
GeenDuizend.appendTransition(
    strings=ziljoenen,
    condition="digits>=(index+1)*6+3",
    nextPoint=Ilj,
    nextDigits="min(digits,(index+2)*6)",
    value="exponent=(index+1)*6")

Ilj.appendTransition(strings=ilj, nextPoint=OenOfArd)

OenOfArd.appendTransition(
    strings=oenard,
    condition="digits%6+index*3<6",
    nextPoint=Grond,
    nextDigits="(digits-3)//6*6+index*3",
    value="total+=mantissa*10**(exponent+index*3);mantissa=0")

#de Stop state heeft ook een overgang: maakt de code makkelijker
Stop.appendTransition(strings=['.'],nextPoint=Stop)

####Maak een state machine
#Dit is een soort van Wijngaarden grammatica:
#de regels zijn een State, de metaregels zijn een Point
class State:
    """Een state voor de state machine die getallen maakt.
    Een state bestaat uit een punt en een aantal cijfers.
    (Dit betekent: de getallen die je kan maken vanaf het punten
    met het gegeven aantal cijfers.)
    Kan worden gehasht zodat je er een set van kan maken.
    """
    def __init__(self, point=Getal, digits=66):
        self.point = point
        self.digits = digits

    def __repr__(self):
        return self.point.name+`self.digits`

    #zorg dat je ze als index kan gebruiken
    def __hash__(self):
        return hash(self.point.name+`self.digits`)

    def __eq__(self, other):
        return self.point.name==other.point.name and self.digits==other.digits

    def nextStates(self, string):
        """Geef de states die voor de gegeven string volgen.
        Pas op: volgt alleen expliciete epsilonovergangen"""
        for t in self.point.getTransitions(string):
            #controleer of aan de voorwaarden is voldaan
            vars = {
                'digits':self.digits,
                'index':t.strings.index(string)}
            if eval(t.condition, vars):
                yield State(t.nextPoint, eval(t.nextDigits, vars))

    def count(self):
        """Geef hoeveel combinaties vanaf deze state kunnen worden gemaakt."""
        vars = {
            'digits':self.digits,
            's':(10**self.digits-1)//999}
        return eval(self.point.occurrence,vars)

    def checkCount(self, verbose=1):
        """Voor debug:
        Controleer de telling van deze state en de volgende."""
        #Eerst maar eens narekenen
        total = 0
        if not eval(self.point.checkCondition, {'digits': self.digits}): return
        for string in self.point.getAllTransitionStrings():
            for state in self.nextStates(string):
                total += state.count()
        if total != self.count() or verbose==2:
            countString = self.formatNumber(self.count())
            totalString = self.formatNumber(total)
            print "State %s geeft %s, en het totaal is %s:"%(
                self, countString, totalString)
            for string in self.point.getAllTransitionStrings():
                for state in self.nextStates(string):
                    print "   Overgang met %s naar %s geeft %s"%(
                        string or "<eps>",
                        state,
                        self.formatNumber(state.count()))
        elif verbose>0:
            print "Point %s klopt: %s"%(self,self.formatNumber(total))

    def formatNumber(self, n):
        """Hulpfunctie voor checkCount: laat getallen mooi zien,
        gebruik makend van s of tienmachten."""
        if abs(n)<10**6: return '%d'%n
        s = (10**self.digits-1)//999
        qs = (n+s//2)//s
        rs = n-qs*s
        logd = len(str(n))-len(str(n)[:4].rstrip('0'))
        d = 10**logd
        qd = (n+d//2)//d
        rd = n-qd*d
        if abs(rd)<abs(rs): name, q, r = '10**'+str(logd), qd, rd
        else: name, q, r = 's', qs, rs
        result = ""
        if q:
            if q==-1: result += '-'
            elif q!=1: result += '%d'%q
            if result and name[0].isdigit(): result += '*'
            result += name
            if r<0: result += '-%d'%-r
            elif r>0: result += '+%d'%+r
        else: result = '%d'%r
        return result

####Maak een niet-deterministische state machine
class FuzzyState:
    def __init__(self, stateSet = None):
        """Maak een niet-deterministische state.
        """
        if stateSet is None: stateSet = set([State()])
        self.stateSet = stateSet

    @staticmethod
    def iterClosure(input):
        """Genereer de transitieve afsluiting onder epsilon."""
        input = input.copy()
        output = set()
        while input:
            #pak een state
            state = input.pop()
            #hebben we die al gehad?
            assert state not in output, "Dubbel pad"
            yield state
            output.add(state)
            #voeg opvolgers toe om nog te proberen
            for nextState in state.nextStates(""): input.add(nextState)

    def getNextState(self, string):
        """Bereken de volgende fuzzy state, gegeven een string.
        [Dit kan sneller als je alles zelf doorpluist: wel lastiger!]
        """
        newState = set()
        for state in self:
            addedStates = set(state.nextStates(string))
            assert not addedStates&newState, "Dubbel pad in graaf"
            newState.update(addedStates)
        return FuzzyState(newState)

    def __repr__(self):
        return 'fuzzy state '+','.join(`sd` for sd in self.stateSet)

    def __iter__(self):
        """Itereer over de _afsluiting_ van de state set."""
        return FuzzyState.iterClosure(self.stateSet)

    def getTransitions(self):
        """Geef alle overgangen aan, en of het een eindstate is.
        Als de lege string erbij zit, is het een einde.
        Gebruikt endFlag in plaats van endState() om tijd te sparen.
        """
        result = set()
        for state in self:
            result.update(state.point.getAllTransitionStrings())
        result.discard('')
        return result

    def endState(self):
        """Geef aan of het getal hier kan stoppen."""
        return any(state.point is Stop for state in self)

    def count(self):
        """Tel hoeveel paden er zijn vanaf hier naar Stop.
        """
        return sum(state.count() for state in self.stateSet)

####gewijzigde state machine die ook de waarde uitrekent
class StateWithValue(State):
    """Uitbreiding van een State met een waarde.
    De waarde wordt berekend door de "value" van de verschillende
    states in volgorde uit te rekenen.
    De meeste overgangen stoppen een waarde in mantissa;
    exponent wordt gezet door de overgang naar Ilj.
    Bijwerken van total gebeurt op de overgang naar Grond.
    """
    def __init__(self, point=Getal, digits=66, localvars = (0, None, None)):
        State.__init__(self, point, digits)
        self.total, self.mantissa, self.exponent = localvars

    def __repr__(self):
        return 'State %s: %s*10**%s+%d'%(
            State.__repr__(self),
            self.mantissa or '?',
            self.exponent or '?',
            self.total)

    def nextStates(self, string):
        """Geef de states die voor de gegeven string volgen.
        Pas op: volgt alleen expliciete epsilonovergangen"""
        for t in self.point.getTransitions(string):
            #controleer of aan de voorwaarden is voldaan
            vars = {
                'digits':self.digits,
                'index':t.strings.index(string),
                'total': self.total,
                'mantissa': self.mantissa,
                'exponent': self.exponent}
            if eval(t.condition, vars):
                #voeg de gegevens toe om er een StateWithValue van te maken
                try:
                    eval(t.value, vars)
                except:
                    print "self", self
                    print "string", string
                    raise
                yield StateWithValue(t.nextPoint, eval(t.nextDigits, vars),
                    (vars['total'], vars['mantissa'], vars['exponent']))

class FuzzyStateWithValue(FuzzyState):
    """Uitbreiding van een fuzzy state met een waarde."""
    def __init__(self, stateSet = None):
        """Maak een niet-deterministische state.
        """
        if stateSet is None: stateSet = set([StateWithValue()])
        self.stateSet = stateSet

####Getallen met een string en waarde representatie
class Number:
    """Een getal dat zowel in tekst als numeriek kan worden weergegeven.
    Gebruik str of int om naar een van de representaties te converteren.
    """
    def __init__(self, integer=None, string=None):
        if isinstance(integer, str):
            assert string is None, "Number verwacht eerst een integer en dan een string"
            integer, string = None, integer
        if string==None:
            string = Number.int2str(integer)
        if integer==None:
            integer = Number.str2int(string)
        self.string = string
        self.integer = integer

    def __repr__(self): return 'Number(%d,"%s")'%(self.integer, self.string)
    def __str__(self): return self.string
    def __int__(self): return self.integer

    @staticmethod
    def mantissa2str(mantissa, expString=""):
        """Bouw een onderdeel van een getalstring van mantissa (een getal) en expString (een string).
        Omdat hij onderdelen produceert, maakt hij geen "nul" en "een"!
        """
        result = ""
        if mantissa==0: return result
        if mantissa>1:
            m2 = mantissa//100%10
            if m2:
                if m2>1:
                    result += tweeTmNegen[m2-2]
                result += "honderd"
            m2 = mantissa%100
            if m2<13: result += (niks+een+tweeTmNegen+tien+elfTwaalf)[m2]
            elif m2<20: result += derTmNegen[m2-13]+"tien"
            else:
                if m2%10:
                    result += (een+tweeTmNegen)[m2%10-1]
                    result += "en"
                result += twinTmNegen[m2//10-2]
                result += "tig"
        return result+expString

    @staticmethod
    def int2str(n):
        """Maak van een integer een string."""
        result = ""
        if n<2: return (nul+een)[n] #uitzonderingen
        elif 1000<n<10000 and n//100%10:
            #jaartallen
            return Number.mantissa2str(n//100,"honderd")+Number.mantissa2str(n%100)
        for exponent in range(63,-3,-3):
            if exponent>3:
                expString = ziljoenen[exponent//6-1]
                expString += "ilj"
                expString += oenard[exponent%6//3]
            elif exponent==3:
                expString = "duizend"
            else: expString = ""
            result += Number.mantissa2str(n//10**exponent%1000, expString)
        return result

    @staticmethod
    def str2int(numberString):
        """Vertaal een string in een getal.
        """
        assert numberString, "Lege string is geen getal"
        state = FuzzyStateWithValue()
        while numberString:
            transitions = state.getTransitions()
            for i in range(1,longestSubstring+1):
                if numberString[:i] in transitions: break
            else: raise ValueError, "String is geen getal"
            state = state.getNextState(numberString[:i])
            numberString = numberString[i:]
        state = state.getNextState('.')
        assert len(state.stateSet)==1, "Beginstuk van een getal"
        return state.stateSet.pop().total

####de echte class: het virtuele array
class AlphaNumbers:
    """Deze klasse maakt virtuele arrays van alle getallen.
    Alle instanties zijn hetzelfde.
    De elementen van het array zijn Numbers (zie hierboven).
    """
    def __len__(self): return 10**66

    def __getitem__(self, n):
        "Haal het n-de alfabetische getal op."
        assert 0<=n<10**66, 'index out of bounds: '+str(n)
        state = FuzzyStateWithValue()
        result = ""
        while True:
            for string in sorted(state.getTransitions()):
                nextState = state.getNextState(string)
                count = nextState.count()
                if n<count:
                    #dit is de goede string, ga een stap dieper
                    state = nextState
                    #kijk of we helemaal klaar zijn
                    if string=='.':
                        assert len(state.stateSet)==1
                        return Number(state.stateSet.pop().total, result)
                    result += string
                    break
                else:
                    #probeer volgende overgang
                    n -= count
            else:
                raise AssertionError, "Internal error in array"

    def bisect(self, s):
        """Zoek het eerste getal dat groter is dan of gelijk is aan s (alfabetisch).
        Geeft de index terug.
        """
        state = FuzzyStateWithValue()
        index = 0
        while True:
            for string in sorted(state.getTransitions()):
                nextState = state.getNextState(string)
                count = nextState.count()
                relevantS = s[:len(string)]
                if relevantS<string:
                    #we zijn er voorbij
                    return index
                elif relevantS==string:
                    #dit is de goede string, ga een stap dieper
                    state = nextState
                    #knip het gevonden stuk eraf
                    s = s[len(string):]
                    #kijk of we helemaal klaar zijn
                    if not s: return index
                    break
                else:
                    #probeer volgende overgang
                    index += count
            else:
                return index

class CachedState(FuzzyState):
    """Snelle versie van FuzzyState door middel van caching.
    De cache is een directory van CachedStates.
    Voor iedere Cachedstate worden de overgangen gegeven als keys
    van een directory. Alle overgangen staan hier in.
    Per overgang wordt de volgende state aangegeven, als die is opgevraagd.
    Heeft weinig nut: de nieuwe states zijn wel snel te bepalen,
    maar je zoekt je nog steeds lens naar de overgangsformules.
    Hiervoor moet hij de berekeningen meenemen.
    Een andere verbetering is dat we alle states in deze cache stoppen,
    desnoods met None als dictionary,
    zodat er geheugen kan worden bespaard.
    Wordt niet gebruikt: ik vermoed ook dat het niet helpt.
    """
    stateCache = {}
    def __init__(self, *args):
        """Maak van FuzzyState"""
        FuzzyState.__init__(self, *args)

    #zorg dat we onszelf als directory index kunnen gebruiken
    def __hash__(self):
        return reduce(xor, map(hash, self.stateSet))
    def __eq__(self, other):
        return self.stateSet==other.stateSet

    def __repr__(self):
        return 'cached '+FuzzyState.__repr__(self)

    def getNextState(self, string):
        result = CachedState.stateCache.get(self, {}).get(string)
        if result is None:
            #bepaal eerst de overgangen, om de dictionary op te zetten
            if self not in CachedState.stateCache:
                d = dict.fromkeys(FuzzyState.getTransitions(self))
                CachedState.stateCache[self] = d
            assert string in CachedState.stateCache[self], 'string not in transitions'
            result = CachedState(FuzzyState.getNextState(self, string).stateSet)
            #bespaar geheugen door een identieke state te zoeken
            #dit werkt niet altijd, omdat hij nog niet in de cache hoeft te zitten
            if result in CachedState.stateCache:
                l = CachedState.stateCache.keys()
                result = l[l.index(result)]
            #sla op voor de volgende keer
            CachedState.stateCache[self][string] = result
        return result

    def getTransitions(self):
        result = CachedState.stateCache.get(self)
        if result is None:
            d = dict.fromkeys(FuzzyState.getTransitions(self))
            CachedState.stateCache[self] = d
            result = d.keys()
        return result

            
alphaNumbers = AlphaNumbers()

if __name__=='__main__':
    def stateTest():
        """test alle states"""
        for name, state in globals().items():
            if isinstance(state, Point):
                for i in range(3,69,3): State(state,i).checkCount(0)

    def conversionTest():
        """Test conversies van getallen."""
        for i in range(100)+range(100,1000,13):
            assert Number(Number(i).string).integer==i
        from random import randrange
        for i in range(10):
            r = randrange(10**66)
            assert Number(Number(r).string).integer==r

    def startStrings(s=6):
        """Produceer (start, end, alphaNumbers[start])
        waar alphaNumbers[start:end] de s eerste letters gelijk heeft.
        """
        end = 0
        while end<10**66:
            start = end
            string = str(alphaNumbers[start])
            if len(string)<s: end = start+1
            else: end = alphaNumbers.bisect(string[:s]+'zzz')
            yield start, end, string

    def distribution(s=3):
        print "Getallen gegroepeerd op %d beginletters:"%s
        for start,end,string in startStrings(s):
            print '%-*s %66d'%(s, string[:s], end-start)

    #doe de test
    stateTest()
    conversionTest()
    print "Test completed."
    distribution()
